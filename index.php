<?php
/**
 * Created by PhpStorm.
 * User: nicolo
 * Date: 16/01/19
 * Time: 16.03
 */

    $serverName = "localhost";
    $username = "nicolo";
    $password = "root";
    $db = "consultingRoom";

    try {
        $conn = new PDO("mysql:host=$serverName;dbname=$db", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        $creaTabella = $conn->exec("CREATE TABLE IF NOT EXISTS STUDENTE (
          matr int(10) NOT NULL,
          nome varchar(30) NOT NULL ,
          cognome varchar (30) NOT NULL ,
          pwd varchar(30) NOT NULL,
          fotoProfilo varchar(30) NOT NULL,
          PRIMARY KEY (matr)
)");
        $conn = null;
    } catch (PDOException $e) {
        echo $sql."<br>".$e->getMessage();
}